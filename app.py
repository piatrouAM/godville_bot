import src.hero as h
import time
from dotenv import load_dotenv
import os
import logging
import sys

logger = logging.getLogger('godville_bot')
if os.getenv('ENV', None) == 'production':
    logger.setLevel(logging.INFO)
else:
    logger.setLevel(logging.DEBUG)
log_handler = logging.StreamHandler(sys.stdout)
log_handler.setFormatter(logging.Formatter('%(asctime)s [%(name)s] [%(levelname)s] %(message)s'))
logger.addHandler(log_handler)

logger.info('Process started')

load_dotenv()
myhero = h.Hero(
    username=os.getenv('username'),
    password=os.getenv('password'),
    apikey=os.getenv('apikey'),
    useragent=os.getenv('useragent')
)

logger.info("{} logined!".format(os.getenv('username')))

def iteration():
    info = myhero.info()
    if 'expired' in info:
        logger.info('updating data')
        myhero.update()
        return iteration()

    if info['health'] == 0 or info['health']/info['max_health'] > 0.25:
        logger.info('health = 0 or > 25%')
        return

    if info['godpower'] >= 25:
        logger.info('make good')
        myhero.make_good()
        return
    
    logger.info('No power!')

while True:
    logger.debug('start iteration')
    iteration()
    logger.debug('stop iteration')
    time.sleep(1800)
