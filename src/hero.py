import requests
import re
import random
import math
import urllib.request as req
import json

webSession = requests.Session()

class Hero:
    WEB_LOGIN = 'https://godville.net/login/login'
    WEB_HEROSCRIPT = 'https://godville.net/javascripts/superhero_ru_packaged.js'
    WEB_ACTION = 'https://godville.net/fbh/feed'
    WEB_API = 'https://godville.net/gods/api'
    HEADERS = {'User-Agent':'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:90.0) Gecko/20100101 Firefox/90.0'}
    web_session = requests.Session()
    login = False
    keys = {}

    def __init__(
            self, 
            username, 
            password, 
            apikey, 
            godname = None, 
            useragent='Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:90.0) Gecko/20100101 Firefox/90.0'
    ) -> None:
        self.HEADERS['User-Agent'] = useragent
        self.apikey = apikey
        self.username = username
        self.password = password
        if godname != None:
            self.godname = godname
        else:
            self.godname = username
        self.login()
        self.update()

    def login(self):
        self.web_session.post(
            self.WEB_LOGIN,
            data={
                'username': self.username,
                'password': self.password,
                'save_login': 'true',
                'commit': 'Войти!',
            },
            headers=self.HEADERS
        )
        self.login = True

    
    def update(self):
        self.web_session.get('https://godville.net/superhero', headers=self.HEADERS)
        self.key_dict()
        self.parse_good_a()

    def make_good(self) -> dict:
        result = self.web_session.post(self.WEB_ACTION, data={
            'a': self.rm(self.good_a),
            'b': self.sm(json.dumps({'action': 'encourage'}, separators=(',', ':')))
        })
        if result.status_code !=200:
            return None
        return result.json()
    
    def key_dict(self) -> None:
        hero_script = self.web_session.get(self.WEB_HEROSCRIPT, headers=self.HEADERS)
        if hero_script.status_code != 200:
            self.login = False
            return
        self.hero_script = hero_script.text
        dict_re = re.compile('var a={([^}]*)}')
        a = dict_re.findall(self.hero_script)
        keys = ''
        for i in a: 
            if len(i) > len(keys):
                keys = i
        keys = keys.replace("\"","")
        for key in keys.split(','):
            pair = key.split(":")
            self.keys[pair[0]] = pair[1]
    
    def parse_good_a(self) -> None:
        if not self.login:
            return
        key_re = re.compile('\$\.ajax\({type:"POST",url:i\.Qm,data:{a:i\.Rm\("([^"]*)"\),b:i\.Sm\(JSON\.stringify\(n\)\)},success:function\(e\)')
        keys = key_re.findall(self.hero_script)
        if len(keys) > 0:
            self.good_a = keys[0]
    
    def info(self):
        result = requests.get("{}/{}/{}".format(self.WEB_API, self.godname, self.apikey))
        if result.status_code != 200:
            return None
        return result.json()

    def i(self):
        e = math.floor(62*random.random())
        if 10 > e:
            return e
        else:
            if 36 > e:
                return chr(e + 55)
            else:
                return chr(e + 61)

    def s(self,e):
        t = ''
        while e > len(t):
            t += str(self.i())
        return t

    def rm(self,e):
        return self.s(4) + self.keys[e] + self.s(5)

    def vk(self, e):
        n = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
        def i(t):
            i = []
            s = 0
            t = req.pathname2url(t)
            n = len(t)
            while n > s:
                a = t[s]
                s += 1
                if "%" != a:
                    i.append(ord(a))
                else:
                    a = t[s] + t[s + 1]
                    i.append(int(a, 16))
                    s+=2
            return i
        t = "="
        a = ""
        o = i(e)
        r = len(o)
        s = 0
        while r - 2 > s:
            a += n[o[s] >> 2]
            a += n[((3 & o[s]) << 4) + (o[s + 1] >> 4)]
            a += n[((15 & o[s + 1]) << 2) + (o[s + 2] >> 6)]
            a += n[63 & o[s + 2]]
            s +=3
        if r % 3:
            s = r - r % 3
            a += n[o[s] >> 2]
            if r % 3 == 2:
                a += n[((3 & o[s]) << 4) + (o[s + 1] >> 4)]
                a += n[(15 & o[s + 1]) << 2]
                a += t
            else:
                a += n[(3 & o[s]) << 4]
                a += t + t
        return a        
    
    def sm(self, e):
        return self.s(5) + self.vk(e) + self.s(3)